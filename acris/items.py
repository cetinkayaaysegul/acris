# -*- coding ## utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in ##
# http ##//doc.scrapy.org/en/latest/topics/items.html

import scrapy


class AcrisItem(scrapy.Item): ##
    # define the fields for your item here like ##
    # name = scrapy.Field()
	address = scrapy.Field()  ## Parcels -> Property Address
	apn = scrapy.Field() ## 
	block = scrapy.Field() ## Parcels ->  Block
	city = scrapy.Field() ## Parcels -> Borough
	county = scrapy.Field() ## Borough
	description = scrapy.Field() ## leave blank
	doc_number = scrapy.Field() ## Document ID
	doc_type = scrapy.Field() ## Doc. Type
	gcp_link = scrapy.Field() ## Leave blank
	lot = scrapy.Field() ## Lot
	name = scrapy.Field() ## Name
	record_date = scrapy.Field() ## Recorded/Filed
	role = scrapy.Field() ## If Party 1, Role is Grantor; If Party 2, Role is Grantee
	sec = scrapy.Field() ## Leave blank
	state = scrapy.Field() ## NY
	transfer_amount = scrapy.Field() ## Doc. Amount
	zipcode = scrapy.Field() ## Leave blank