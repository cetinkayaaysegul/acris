# -*- coding: utf-8 -*-
import scrapy
from acris.items import AcrisItem
import json

class AcrisSpider(scrapy.Spider):
    name = "acris"
    start_urls = ['https://a836-acris.nyc.gov/DS/DocumentSearch/DocumentType']
    
    def parse_date(self, date):
        ### need to check length and if it is a proper date etc. 
        ### will implement the checks later
        return { 'mm': date[:2], 'dd': date[2:4], 'yyyy': date[4:8] }

    def extract_next(self, details_list, key):
        ### finds key in list returns the next element in the list
        if key in details_list and len(details_list) > details_list.index(key):
            return details_list[details_list.index(key) + 1]
        else:
            return ''

    def nonempty_strings(self, string_list):
        ## returns a list of non-empty strings in a list
        return [i.strip() for i in string_list if i.strip()]

    def acris_table_header(self, response, table_name):
        ## returns header row of table in acris document page
        return self.nonempty_strings(response.xpath('//*[contains(text(),"'+ table_name +
            '")]/ancestor::table[1]/descendant::table[1]//text()').extract())   

    def acris_table_data(self, response, table_name):
        ## returns table data in acris document page as a list of list
        ## using this format to recover empty data fields 
        return [self.nonempty_strings(i.xpath('*//text()').extract()) 
            for i in response.xpath('//*[contains(text(),"'+ table_name +'")]/ancestor::table[1]/descendant::table[2]//td')]

    def acris_table_list(self, response, table_name):
        ## returns table data in acris document page as list of dictionaries, format:
        ### [{u'ADDRESS 1': u'.....ST AVENUE', u'ADDRESS 2': '', u'CITY': u'WHITESTONE'...
        table_header = self.acris_table_header(response, table_name)

        ### flatten table data recovering empty fields
        table_data = []
        for i in self.acris_table_data(response, table_name):
            table_data.append(i[0]) if i else table_data.append('') 
        
        ### divide table data into chunks
        table_data_chunks = [table_data[x:x+len(table_header)] for x in range(0, len(table_data), len(table_header))]

        #### each table row is a separate dictionary, function return a list of rows(dictionaries)
        return [{x: chunk[i] for i, x in enumerate(table_header)} for chunk in table_data_chunks if chunk[0]]

    def parse(self, response):
        ### parses document types and initiate requests for records for all document types
        
        ### find document types using jsonDocumentTypes js object within html file
        script_text = response.xpath('//script[contains(text(),"jsonDocumentTypes")]/text()').extract_first(default='')
        if '=' in script_text:
            json_list = script_text.split('=')[1:]
            json_text = '='.join(json_list).strip()
            document_types_list = json.loads(json_text)
        else:
            document_types_list = []

        ### to access cookies in scrapy shell:
        #cookie_list = [i.split('=') for i in request.headers['Cookie'].split(';')]
        ### to access cookies via spider:
        cookie_list = [i.split('=') for i in response.headers.getlist('Set-Cookie')[0].split(';')]
        cookie_dict = { i[0].strip() : '='.join([j.strip() for j in i[1:]])for i in cookie_list}
        request_verification_token = cookie_dict['__RequestVerificationToken_L0RT']

        ### formatting date strings as dictionaries such as {'mm':'12', 'dd':'02', 'yyyy':2015}
        from_date = self.parse_date(self.from_date)
        to_date = self.parse_date(self.to_date)

        ### request document records between dates, for all document types
        for document_type in document_types_list:
            formdata = {'hid_doctype': document_type['Type'],
                    'hid_doctype_name':document_type['Description'].replace(' ','+'),
                    'hid_selectdate':'DR',
                    'hid_datefromm': from_date['mm'],
                    'hid_datefromd': from_date['dd'],
                    'hid_datefromy': from_date['yyyy'],
                    'hid_datetom' : to_date['mm'],
                    'hid_datetod' : to_date['dd'],
                    'hid_datetoy' : to_date['yyyy'],
                    'hid_borough' :'0',
                    'hid_borough_name':'ALL+BOROUGHS',
                    'hid_max_rows':'99',
                    'hid_page':'1',
                    'hid_ReqID':'',
                    'hid_SearchType':'DOCTYPE',
                    'hid_ISIntranet':'N',
                    'hid_sort':'',
                    '__RequestVerificationToken':request_verification_token}

            ####request 99 records per result page
            request = scrapy.http.FormRequest('https://a836-acris.nyc.gov/DS/DocumentSearch/DocumentTypeResult', 
                formdata= formdata, callback=self.parse_search)
            request.meta['formdata'] = formdata
            request.meta['page'] = '1'
            yield request

    def parse_search(self, response):
        ### parses search results and initates requests for the next page of search results
        ### for each document id. requests the document details page
        
        ### recover current page and original form data to use in future requests
        formdata = response.meta['formdata']
        page = response.meta['page']

        ##check if next_page exists and start request for next page if available
        if response.xpath('//a[contains(@href, "go_next")]').extract_first(default=''):
            ### increment page number to request next_page
            next_page= str(int(page) + 1)
            request = scrapy.http.FormRequest('https://a836-acris.nyc.gov/DS/DocumentSearch/DocumentTypeResult?page='+next_page, 
            formdata= formdata, callback=self.parse_search)
            request.meta['formdata'] = formdata
            request.meta['page'] = next_page
            yield request

        ### extract document ids and request document pages
        for document_id in response.xpath('//input[@name="DET"]/@onclick').re('go_detail\("(\d+)"\)'):
            document_request = scrapy.Request('https://a836-acris.nyc.gov/DS/DocumentSearch/DocumentDetail?doc_id='+document_id, 
                 callback=self.parse_document)
            yield document_request

    def parse_document(self, response):
        ###parses document details page to create rows in output csv file

        ### create a list of strings from the data fields in the document details table
        document_details = self.nonempty_strings([' '.join(self.nonempty_strings(i.xpath('*//text()').extract()))
            for i in response.xpath('//*[contains(text(),"DOCUMENT ID")]/ancestor::table[1]//td')])

        ### create a dictionary with document details that we want to extract
        document_dict = { keyword: self.extract_next(document_details, keyword)
            for keyword in ['BOROUGH:','DOCUMENT ID:','DOC. TYPE:','RECORDED / FILED:','DOC. AMOUNT:']}

        ### create list of parties and assigns their roles
        party_1_list = self.acris_table_list(response,'PARTY 1')
        for party_1 in party_1_list:
            party_1['ROLE'] = 'Grantor'

        party_2_list = self.acris_table_list(response,'PARTY 2')
        for party_2 in party_2_list:
            party_2['ROLE'] = 'Grantee'

        ### create a list of parcels 
        parcels_list = self.acris_table_list(response,'PARCELS')    

        ### loop through parcels and parties and create rows/items in our output file
        for parcel in parcels_list:
            for party in party_1_list + party_2_list:
                item = AcrisItem()
                item['address'] = parcel['PROPERTY ADDRESS']  ## Parcels -> Property Address
                item['apn'] = '' ## 
                item['block'] = parcel['BLOCK'] ## Parcels ->  Block
                item['city'] = parcel['BOROUGH'] ## Parcels -> Borough
                item['county'] = document_dict['BOROUGH:'] ## Borough
                item['description'] = '' ## leave blank
                item['doc_number'] = document_dict['DOCUMENT ID:'] ## Document ID
                item['doc_type'] = document_dict['DOC. TYPE:'] ## Doc. Type 
                item['gcp_link'] = '' ## Leave blank
                item['lot'] = parcel['LOT'] ## Lot
                item['name'] = party['NAME'] ## Name
                item['record_date'] = document_dict['RECORDED / FILED:'] ## Recorded/Filed
                item['role'] = party['ROLE'] ## If Party 1, Role is Grantor; If Party 2, Role is Grantee
                item['sec'] = '' ## Leave blank
                item['state'] = 'NY' ## NY
                item['transfer_amount'] = document_dict['DOC. AMOUNT:'] ## Doc. Amount
                item['zipcode'] = '' ## Leave blank
                yield item
